package com.mmm.ej.gof.creational.factory.method.notification;

public class NotificationMain {

    public static void main(String[] args) {

        String subject = "Election result published";
        String text = "Agencies just announced that the winner of elections is" +
                " the candidate of Democrats/Republicans";
        //NotificationProvider notificationSender = new NotificationProvider();
        Notification notification = NotificationProvider.createNotification(NotificationType.ONE, subject, text);
        notification.emit(); // sending

    }
}
