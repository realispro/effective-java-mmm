package com.mmm.ej.gof.creational.factory.method.beer;

public class BeerMain {

    public static void main(String[] args) {

        Bar bar = new Bar();
        Beer beer = bar.getBeer();
        System.out.println("drinking beer " + beer.getName() + " volatge " + beer.getVoltage());

    }
}
