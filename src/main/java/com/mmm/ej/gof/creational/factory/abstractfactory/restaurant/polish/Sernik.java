package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.polish;

import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Sernik implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("enjoying sernik");
    }
}
