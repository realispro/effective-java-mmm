package com.mmm.ej.gof.creational.factory.method.beer;

public interface Beer {

    float getVoltage();

    String getName();

}
