package com.mmm.ej.gof.creational.factory.method.beer;

public class ZywiecIpa implements Beer{
    @Override
    public float getVoltage() {
        return 5.0f;
    }

    @Override
    public String getName() {
        return "Zywiec IPA";
    }
}
