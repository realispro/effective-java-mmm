package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.french;

import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Pasta implements MainDish {

    @Override
    public void eat() {
        System.out.println("eating pasta");
    }
}
