package com.mmm.ej.gof.creational.factory.method.beer;

public class Radler implements Beer{
    @Override
    public float getVoltage() {
        return 2.2f;
    }

    @Override
    public String getName() {
        return "Radler";
    }
}
