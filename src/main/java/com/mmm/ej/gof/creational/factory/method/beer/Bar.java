package com.mmm.ej.gof.creational.factory.method.beer;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Bar {

    public Beer getBeer(){

        DayOfWeek today = LocalDate.now()/*.plusDays(2)*/.getDayOfWeek();
        if(today==DayOfWeek.SUNDAY || today==DayOfWeek.SATURDAY){
            return new Pilsner();
        } else {
            return new Radler();
        }

    }

    public Beer getBeer(BeerType type){
        switch (type){
            case IPA:
                return new ZywiecIpa();
            case FRUITY:
                return new Radler();
            default:
                return new Pilsner();
        }
    }

}
