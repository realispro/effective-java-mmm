package com.mmm.ej.gof.creational.factory.method.notification;

public enum NotificationType {

    ONE,
    TWO

}
