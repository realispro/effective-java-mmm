package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.french;

import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Pancakes implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("enjoying pancakes");
    }
}
