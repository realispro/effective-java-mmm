package com.mmm.ej.gof.creational.factory.abstractfactory.zoo;

public enum AnimalType {

    VEGE,
    PREDATOR
}
