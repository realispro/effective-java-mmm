package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant;

public interface MainDish {

    void eat();
}
