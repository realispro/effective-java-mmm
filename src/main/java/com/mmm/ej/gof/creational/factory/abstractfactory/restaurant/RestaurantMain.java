package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant;

import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.french.LaCuisine;
import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.polish.PolskieJadlo;

public class RestaurantMain {


    public static void main(String[] args) {
        System.out.println("Lunch time!");

        Restaurant restaurant = new LaCuisine();

        MainDish mainDish = restaurant.getMainDish();
        mainDish.eat();

        Dessert dessert = restaurant.getDessert();
        dessert.enjoy();
    }
}
