package com.mmm.ej.gof.creational.singleton.sample;

public enum EnumSingleton {

    INSTANCE;

    public static EnumSingleton getInstance(){
        return INSTANCE;
    }

    int state = 2;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
