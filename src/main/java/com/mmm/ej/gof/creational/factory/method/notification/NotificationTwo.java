package com.mmm.ej.gof.creational.factory.method.notification;

public class NotificationTwo implements Notification {
    
    private String title;
    
    private String description;
    
    NotificationTwo(String title, String description){
        this.title = title != null ? title : "Subject no 1";
        this.description = description != null ? description : "Description no 1";
        /*emit();*/
    }
    
    /*public NotificationOne(){
        this("Subject no 1", "Description no 1");
    }*/
    
    @Override
    public String getSubject() {
        return title;
    }
    
    @Override
    public String getDescription() {
        return description;
    }
    
    @Override
    public void emit() {
        System.out.println("notification2 is: " + getSubject()  +" " +  getDescription());
    }
}
