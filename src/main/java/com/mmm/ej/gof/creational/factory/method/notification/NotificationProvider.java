package com.mmm.ej.gof.creational.factory.method.notification;

public class NotificationProvider {

    public static Notification createNotification(NotificationType type, String title, String subject) {
        switch (type) {
            case ONE:
                return new NotificationOne(title, subject);
            default:
                return new NotificationTwo(title, subject);
        }
    }
}
