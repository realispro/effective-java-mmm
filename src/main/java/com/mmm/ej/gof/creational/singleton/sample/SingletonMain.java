package com.mmm.ej.gof.creational.singleton.sample;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonMain {

    public static void main(String[] args) {

        EnumSingleton singleton = EnumSingleton.getInstance();
        EnumSingleton singleton2 = EnumSingleton.INSTANCE;

      /*  Class clazz = EnumSingleton.class;
        try {
            Constructor c = clazz.getDeclaredConstructor();
            c.setAccessible(true);
            Object o = c.newInstance();
            singleton2 = (EnumSingleton)o;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }*/


        System.out.println("same? " + (singleton==singleton2));

    }
}
