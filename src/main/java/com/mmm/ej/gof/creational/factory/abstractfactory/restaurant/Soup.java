package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant;

public interface Soup {

    void drink();
}
