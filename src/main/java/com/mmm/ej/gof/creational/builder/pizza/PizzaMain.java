package com.mmm.ej.gof.creational.builder.pizza;

public class PizzaMain {

    public static void main(String[] args) {

        PizzaBuilder pizzaBuilder = new PizzaBuilder();
        Pizza pizza = pizzaBuilder
                .setDough("grube")
                .addTopping("szynka")
                .addTopping("pomidor")
                .setSause("czosnkowy")
                .bake();

                /*new Pizza();
        pizza.setDough("cienkie");
        pizza.addTopping("tunczyk");
        pizza.addTopping("ananas");
        pizza.addTopping("keczup");*/


        System.out.println("eating pizza: " + pizza);

    }
}
