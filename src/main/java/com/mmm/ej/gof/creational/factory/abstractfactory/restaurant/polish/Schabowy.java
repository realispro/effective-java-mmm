package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.polish;

import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Schabowy implements MainDish {
    @Override
    public void eat() {
        System.out.println("eating schabowy");
    }
}
