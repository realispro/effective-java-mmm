package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant;

public enum RestaurantType {

    MCDONALDS,
    MEXICAN,
    ITALIAN
}
