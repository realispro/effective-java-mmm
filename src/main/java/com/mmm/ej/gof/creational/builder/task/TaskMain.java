package com.mmm.ej.gof.creational.builder.task;

public class TaskMain {


    public static void main(String[] args) {

        TaskBuilder taskBuilder = new TaskBuilder();
        Task task = taskBuilder
                .setId(1)
                .setTitle("title")
                .setDescription("decription")
                .doThis();
        System.out.println("task = " + task);
    }
}
