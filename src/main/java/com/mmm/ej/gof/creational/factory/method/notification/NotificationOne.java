package com.mmm.ej.gof.creational.factory.method.notification;

public class NotificationOne implements Notification {

    private String title;

    private String description;

    NotificationOne(String title, String description){
        this.title = title != null ? title : "Subject no 1";
        this.description = description != null ? description : "Description no 1";
        /*emit();*/
    }

    /*public NotificationOne(){
        this("Subject no 1", "Description no 1");
    }*/

    @Override
    public String getSubject() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void emit() {
        System.out.println("notification1 is: " + getSubject()  +" " +  getDescription());
    }
}
