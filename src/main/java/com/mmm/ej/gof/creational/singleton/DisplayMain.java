package com.mmm.ej.gof.creational.singleton;

public class DisplayMain {

    public static void main(String[] args) {


        Display display1 = Display.getInstance();
        Display display2 = Display.INSTANCE;

        display1.show("test display 1");
        display2.show("test display 2");
        System.out.println("same? " + (display1==display2));


    }

}
