package com.mmm.ej.gof.creational.builder.task;

public class TaskBuilder {

    private Task task = new Task();

    public TaskBuilder setId(int id){
        task.setId(id);
        return this;
    }

    public TaskBuilder setDescription(String description){
        task.setDescription(description);
        return this;
    }

    public TaskBuilder setTitle(String title){
        task.setTitle(title);
        return this;
    }

    public Task doThis(){
        return this.task;
    }
}
