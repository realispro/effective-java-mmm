package com.mmm.ej.gof.creational.factory.abstractfactory.zoo;

public interface Bird {

    void fly();
}
