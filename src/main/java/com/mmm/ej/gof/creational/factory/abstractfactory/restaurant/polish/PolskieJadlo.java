package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.polish;

import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.Appetizer;
import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;
import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;
import com.mmm.ej.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class PolskieJadlo implements Restaurant {

    @Override
    public Appetizer getAppetizer() {
        return null;
    }

    @Override
    public MainDish getMainDish() {
        return new Schabowy();
    }

    @Override
    public Dessert getDessert() {
        return new Sernik();
    }
}
