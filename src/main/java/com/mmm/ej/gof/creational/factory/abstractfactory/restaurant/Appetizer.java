package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant;

public interface Appetizer {

    void taste();
}
