package com.mmm.ej.gof.creational.factory.abstractfactory.restaurant;

public interface Dessert {

    void enjoy();
}
