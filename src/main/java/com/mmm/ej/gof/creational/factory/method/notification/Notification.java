package com.mmm.ej.gof.creational.factory.method.notification;

public interface Notification {

    String getSubject();

    String getDescription();

    void emit();

}
