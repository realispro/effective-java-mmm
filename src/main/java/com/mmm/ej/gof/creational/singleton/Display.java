package com.mmm.ej.gof.creational.singleton;

public enum Display {

    INSTANCE;

    public static Display getInstance(){
        return INSTANCE;
    }

    public void show(String text){
        System.out.println("[" + text + "]");
    }
}
