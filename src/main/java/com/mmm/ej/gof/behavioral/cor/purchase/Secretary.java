package com.mmm.ej.gof.behavioral.cor.purchase;

public class Secretary extends Approver {

    public Secretary(String name) {
        super(name);
    }

    @Override
    protected int getPurchaseLimit() {
        return 1000;
    }


}