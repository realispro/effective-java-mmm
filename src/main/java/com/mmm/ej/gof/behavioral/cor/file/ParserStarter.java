package com.mmm.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public class ParserStarter {

    public static void main(String[] args) throws IOException {
        System.out.println("let's parse a file");

        File file = new File("./src/main/resources/cor/cor.txt");
        Parser parser = ParserProvider.getParser(file.getName());
                //getChain();
        parser.parse(file);
    }

    private static Parser getChain(){
        CsvParser csvParser = new CsvParser();
        TxtParser txtParser = new TxtParser();
        csvParser.setNext(txtParser);
        return csvParser;
    }



}
