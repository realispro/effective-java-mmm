package com.mmm.ej.gof.behavioral.observer.press;

import java.util.ArrayList;
import java.util.List;

public class PressOffice {

    // TODO list od subscriber

    public void dayWork(){

        News n1 = new News("Mundial: Poland won!", "Polska mistrzem Polski");
        broadcast(n1);

        News n2 = new News("Chemitrails are fake", "Chemitrails doesn't exists");
        broadcast(n2);
    }

    private void broadcast(News news){
        // TODO send notifications
    }



}
