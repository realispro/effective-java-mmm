package com.mmm.ej.gof.behavioral.cor.file;

import java.util.HashMap;
import java.util.Map;

public class ParserProvider {

    static Map<String, Parser> map = new HashMap<>();

    static {
        map.put("csv", new CsvParser());
        map.put("txt", new TxtParser());
    }

    public static Parser getParser(String fileName){
        String key = map.keySet()
                .stream()
                .filter(e->fileName.endsWith(e))
                .findFirst()
                .get();
        return map.get(key);
    }
}
