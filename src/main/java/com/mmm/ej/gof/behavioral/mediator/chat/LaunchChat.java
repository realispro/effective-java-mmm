package com.mmm.ej.gof.behavioral.mediator.chat;

/**
 * Created by xdzm on 2016-06-17.
 */
public class LaunchChat {

    public static void main(String[] args) {
        ChatPart cp1 = new ChatPart();
        ChatPart cp2 = new ChatPart();
        ChatPart cp3 = new ChatPart();

        cp1.send("Hey man!", cp2);
        cp1.send("Hey man!", cp3);
    }
}
