package com.mmm.ej.gof.behavioral.cor.purchase;


public class Director extends Approver {

    public Director(String name) {
        super(name);
    }

    @Override
    protected int getPurchaseLimit() {
        return 10000;
    }


}
