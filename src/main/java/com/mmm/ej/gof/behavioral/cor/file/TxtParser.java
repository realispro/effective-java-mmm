package com.mmm.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TxtParser extends Parser {

    @Override
    public boolean isApplicable(File file) {
        return file.getName().endsWith(".txt");
    }

    @Override
    public void handle(File file) throws IOException {
        Files
                .lines(Paths.get(file.getAbsolutePath()))
                .forEach(System.out::println);
    }
}
