package com.mmm.ej.gof.behavioral.strategy.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class AESEncrypter implements Encrypter{

    private byte[] keyValue = new byte[]{
            '1', '2', '3', '4', '5', '6', '7', '8',
            '1', '2', '3', '4', '5', '6', '7', '8'
    };

    private String alg = "AES";

    @Override
    public byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(keyValue, alg);
        Cipher c = Cipher.getInstance(alg);
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(s.getBytes());
    }
}
