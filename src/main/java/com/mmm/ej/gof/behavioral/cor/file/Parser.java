package com.mmm.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public abstract class Parser {

    protected Parser next;

    public void setNext(Parser parser){
        this.next = parser;
    }

    public final void parse(File file) throws IOException {
        if(isApplicable(file)) {
            handle(file);
        } else {
            if(next!=null){
                next.parse(file);
            }
        }
    }

    public abstract boolean isApplicable(File file) throws IOException;
    public abstract void handle(File file) throws IOException;
}
