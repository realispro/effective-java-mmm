package com.mmm.ej.gof.behavioral.cor.purchase;

public class PurchaseStart {

    public static void main(String[] args) {
        System.out.println("let's process a purchase");

        Purchase car = new Purchase(243162361, "samochod dla sprzedawcy");
        Purchase paper = new Purchase(30, "papier do drukarki");
        Purchase laptop = new Purchase(8000, "Lenovo Thinkpad");

        Approver approver = getApprover();
        approver.approve(car);
        approver.approve(paper);
        approver.approve(laptop);

        System.out.println("car: " + car);
        System.out.println("paper: " + paper);
        System.out.println("laptop: " + laptop);
    }

    private static Approver getApprover(){
        Secretary secretary = new Secretary("Basiaa");
        Director director = new Director("inz. Karwowski");
        secretary.setNext(director);
        President president = new President("Mr President");
        director.setNext(president);
        return secretary;
    }
}
