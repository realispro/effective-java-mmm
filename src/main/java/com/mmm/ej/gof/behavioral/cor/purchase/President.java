package com.mmm.ej.gof.behavioral.cor.purchase;

public class President extends Approver {

    public President(String name) {
        super(name);
    }

    @Override
    protected int getPurchaseLimit() {
        return -1;
    }


}
