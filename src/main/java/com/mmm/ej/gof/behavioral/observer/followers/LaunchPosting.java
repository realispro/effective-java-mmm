package com.mmm.ej.gof.behavioral.observer.followers;

public class LaunchPosting {

    public static void main(String[] args) {

        Profile profile = new Profile("profile1");
        Profile profile2 = new Profile("profile2");
        Profile profile3 = new Profile("profile3");

        profile.follow(profile2);
        profile.follow(profile3);

        profile.publish("totally unimportant message 1");
        profile.publish("totally unimportant message 2");

    }
}
