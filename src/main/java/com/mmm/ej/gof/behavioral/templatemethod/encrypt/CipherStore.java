package com.mmm.ej.gof.behavioral.templatemethod.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public abstract class CipherStore extends SafeStore {


    public abstract byte[] getKey();

    public abstract String getAlg();

    protected byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(getKey(), getAlg());
        Cipher c = Cipher.getInstance(getAlg());
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(s.getBytes());
    }
}
