package com.mmm.ej.gof.behavioral.templatemethod.encrypt;

public class AESStore extends CipherStore{

    private byte[] keyValue = new byte[]{
            '1', '2', '3', '4', '5', '6', '7', '8',
            '1', '2', '3', '4', '5', '6', '7', '8'
    };

    private String alg = "AES";

    @Override
    public byte[] getKey() {
        return keyValue;
    }

    @Override
    public String getAlg() {
        return alg;
    }
}
