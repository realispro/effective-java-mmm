package com.mmm.ej.gof.behavioral.mediator.chat;

/**
 * Created by xdzm on 2016-06-17.
 */
public class ChatPart {

    public void send(String message, ChatPart cp){
        cp.receive(message);
    }

    public void receive(String message){
        System.out.println("someone send a letter: " + message);
    }
}
