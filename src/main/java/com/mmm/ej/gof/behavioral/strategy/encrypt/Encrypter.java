package com.mmm.ej.gof.behavioral.strategy.encrypt;

public interface Encrypter {
    byte[] encrypt(String s) throws Exception;
}
