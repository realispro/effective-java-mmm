package com.mmm.ej.gof.behavioral.strategy.encrypt;


public class EncryptStarter {

    public static void main(String[] args){
        SafeStore store = new SafeStore(new AESEncrypter());
        boolean stored = store.store("hide it!");
        System.out.println("safely stored? " + stored);
    }

}
