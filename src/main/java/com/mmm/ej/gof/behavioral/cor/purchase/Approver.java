package com.mmm.ej.gof.behavioral.cor.purchase;

public abstract class Approver {

    protected Approver nextApprover;

    private String name;

    public Approver(String name){
        this.name = name;
    }


    public final void approve(Purchase p) {

        if(getPurchaseLimit()==-1 || p.getAmount()<getPurchaseLimit()) {
            System.out.println("Przyjęte do realizacji.");
            p.approve(getName());
        }
        else{
            if(nextApprover!=null) {
                nextApprover.approve(p);
            }
        }
    }

    protected abstract int getPurchaseLimit();

    public String getName() {
        return name;
    }
    public void setNext(Approver nextApprover){
        this.nextApprover = nextApprover;
    }
}
