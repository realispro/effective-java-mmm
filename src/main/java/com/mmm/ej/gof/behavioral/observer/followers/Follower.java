package com.mmm.ej.gof.behavioral.observer.followers;

public interface Follower {

    void notify(Post post);
}
