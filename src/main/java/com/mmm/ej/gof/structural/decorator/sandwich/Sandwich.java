package com.mmm.ej.gof.structural.decorator.sandwich;

public interface Sandwich {

    String content();
}
