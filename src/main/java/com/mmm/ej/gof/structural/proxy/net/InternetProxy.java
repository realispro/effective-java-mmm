package com.mmm.ej.gof.structural.proxy.net;


import java.util.Arrays;
import java.util.List;

public class InternetProxy implements Internet{

    private List<String> blackList = Arrays.asList("facebook.com", "twitter.com");

    private Internet internet/* = new RealNetwork()*/;

    public InternetProxy(Internet internet) {
        this.internet = internet;
    }

    @Override
    public void connectTo(String address) {
        if (!ifAllowed(address)){
            //throw new RuntimeException("access denied for address: " + address);
            System.out.println("access denied for address: " + address);
        }else{
            internet.connectTo(address);
        }
    }

    private boolean ifAllowed(String address){
        return blackList.stream().noneMatch(a->address.contains(a));
    }

}
