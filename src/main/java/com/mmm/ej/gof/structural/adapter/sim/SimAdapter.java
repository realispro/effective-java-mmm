package com.mmm.ej.gof.structural.adapter.sim;

public class SimAdapter implements Sim {

    private MiniSimCard miniSimCard;

    public SimAdapter (MiniSimCard miniSimCard){
        this.miniSimCard = miniSimCard;
    }

    @Override
    public String getId() {
        return new String(miniSimCard.getId());
    }
}
