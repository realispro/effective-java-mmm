package com.mmm.ej.gof.structural.decorator.sandwich;

public class GenericSandwichDecorator implements Sandwich{

    private Sandwich sandwich;

    private String addon;

    public GenericSandwichDecorator(Sandwich sandwich, String addon) {
        this.sandwich = sandwich;
        this.addon = addon;
    }

    @Override
    public String content() {
        return sandwich.content() + ", " + addon;
    }
}
