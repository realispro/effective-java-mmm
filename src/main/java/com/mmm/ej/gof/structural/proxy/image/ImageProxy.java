package com.mmm.ej.gof.structural.proxy.image;

public class ImageProxy implements Image {

    private String fileName;

    public ImageProxy(String fileName){
        this.fileName = fileName;
    }

    @Override
    public byte[] getData() {
        Image i = new ImageFile(fileName);
        return i.getData();
    }
}
