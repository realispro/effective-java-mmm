package com.mmm.ej.gof.structural.flyweight.wallet;

import java.util.ArrayList;
import java.util.List;

public class WalletStarter {

    public static void main(String[] args) {
        System.out.println("let's put some coins into wallet");

        CoinCache coinCache = new CoinCache();
        List<Coin> wallet = new ArrayList<>();
        wallet.add(coinCache.getCoin(5, Currency.PLN));
        wallet.add(coinCache.getCoin(2, Currency.EURO));
        wallet.add(coinCache.getCoin(1, Currency.DOLLAR));
        wallet.add(coinCache.getCoin(5, Currency.PLN));
        wallet.add(coinCache.getCoin(2, Currency.EURO));
        wallet.add(coinCache.getCoin(5, Currency.PLN));

        System.out.println("wallet content: " + wallet);



    }
}
