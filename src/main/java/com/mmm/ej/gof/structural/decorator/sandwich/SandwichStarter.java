package com.mmm.ej.gof.structural.decorator.sandwich;

public class SandwichStarter {

    public static void main(String[] args) {
        System.out.println("let's eat some sandwich");

        Sandwich sandwich =
                new GenericSandwichDecorator(
                new GenericSandwichDecorator(
                    new ButterDecorator(new WhiteBread()),
                    "salami"),
                "cheese");

        System.out.println("eating " + sandwich.content());
    }
}
