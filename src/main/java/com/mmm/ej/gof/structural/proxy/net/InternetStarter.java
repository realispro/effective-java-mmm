package com.mmm.ej.gof.structural.proxy.net;


import com.mmm.ej.gof.structural.proxy.net.internet.RealNetwork;

public class InternetStarter {

    public static void main(String[] args) {
        System.out.println("lets surf!");

        Internet internet = InternetProvider.provide();

        internet.connectTo("wp.pl");
        internet.connectTo("facebook.com/events");
        internet.connectTo("facebook.com/messages");

    }
}
