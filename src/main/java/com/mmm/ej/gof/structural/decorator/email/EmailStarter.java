package com.mmm.ej.gof.structural.decorator.email;

public class EmailStarter {


    public static void main(String[] args) {
        System.out.println("let's send some email");

        Email email =
                new UrgencyDecorator(
                        new SignatureDecorator(
                                new SignatureDecorator(
                                        new EmailMessage(
                                                "USA elections results!",
                                                "what a surprise!"),
                                        "Cheers,\nAlicja!"
                                ),
                                "RODO clause private confidential etc"
                        )
                );

        System.out.println("sending email: \n[" + email.getTitle() + "]\n[" + email.getContent() + "]");

    }

}
