package com.mmm.ej.gof.structural.flyweight.wallet;

import java.util.HashMap;
import java.util.Map;

// Flyweight factory
public class CoinCache {

    private Map<String, Coin> coins = new HashMap<>();

    public Coin getCoin(int value, Currency currency){
        String key = value + currency.name();
        Coin coin = coins.get(key);
        if(coin==null){
            coin = new Coin(value, currency);
            coins.put(key, coin);
        }
        return coin;
    }

}
