package com.mmm.ej.gof.structural.adapter.pressure;

import com.mmm.ej.gof.structural.adapter.pressure.bar.Bar2PascalAdapter;
import com.mmm.ej.gof.structural.adapter.pressure.bar.BarPressureProvider;

public class PressureStarter {

    public static void main(String[] args) {
        System.out.println("let's measure a pressure !");

        PascalPressure pascalPressure = new Bar2PascalAdapter(new BarPressureProvider());
        Display display = new Display();
        display.showPresure(pascalPressure);


    }
}
