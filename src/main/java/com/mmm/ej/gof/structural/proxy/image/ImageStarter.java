package com.mmm.ej.gof.structural.proxy.image;

public class ImageStarter {

    public static void main(String[] args) {
        System.out.println("let's display some images");

        Image i1 = new ImageProxy("image1.png");
        Image i2 = new ImageProxy("image2.png");
        Image i3 = new ImageProxy("image3.png");
        Image i4 = new ImageProxy("image4.png");

        byte[] data = i1.getData();
        System.out.println("displaying image: " + new String(data));
    }
}
