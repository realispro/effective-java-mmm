package com.mmm.ej.gof.structural.decorator.email;

public class SignatureDecorator implements Email{

    private Email email;

    private String signature;

    public SignatureDecorator(Email email, String signature) {
        this.email = email;
        this.signature = signature;
    }

    @Override
    public String getTitle() {
        return email.getTitle();
    }

    @Override
    public String getContent() {
        return email.getContent() + "\n" + signature;
    }
}
