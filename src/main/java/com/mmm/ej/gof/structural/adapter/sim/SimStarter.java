package com.mmm.ej.gof.structural.adapter.sim;

public class SimStarter {

    public static void main(String[] args) {
        System.out.println("let's install sim card");

        MiniSimCard miniSimCard = new MiniSimCard("1234567890");
        Device d = new Device();
        Sim sim = new SimAdapter(miniSimCard);
        d.install(sim);

    }
}
