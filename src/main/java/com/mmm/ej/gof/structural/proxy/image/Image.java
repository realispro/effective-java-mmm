package com.mmm.ej.gof.structural.proxy.image;

public interface Image {

    byte[] getData();

}
