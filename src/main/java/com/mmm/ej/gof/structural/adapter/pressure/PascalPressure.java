package com.mmm.ej.gof.structural.adapter.pressure;

public interface PascalPressure {

    float getPressure();
}
