package com.mmm.ej.gof.structural.decorator.email;


public interface Email {

    String getTitle();

    String getContent();
}
