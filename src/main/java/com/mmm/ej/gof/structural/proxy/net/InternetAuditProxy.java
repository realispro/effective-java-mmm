package com.mmm.ej.gof.structural.proxy.net;

public class InternetAuditProxy implements Internet{

    private Internet internet;

    public InternetAuditProxy(Internet internet) {
        this.internet = internet;
    }

    @Override
    public void connectTo(String address) {
        System.out.println("accessing: " + address);
        this.internet.connectTo(address);
    }
}
