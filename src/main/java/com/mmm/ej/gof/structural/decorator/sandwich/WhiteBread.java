package com.mmm.ej.gof.structural.decorator.sandwich;

public class WhiteBread implements Sandwich{
    @Override
    public String content() {
        return "white bread";
    }
}
