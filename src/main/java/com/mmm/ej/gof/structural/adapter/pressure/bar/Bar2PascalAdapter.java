package com.mmm.ej.gof.structural.adapter.pressure.bar;

import com.mmm.ej.gof.structural.adapter.pressure.PascalPressure;

public class Bar2PascalAdapter implements PascalPressure {

    private BarPressureProvider barPressureProvider;

    public Bar2PascalAdapter(BarPressureProvider barPressureProvider) {
        this.barPressureProvider = barPressureProvider;
    }

    @Override
    public float getPressure() {
        return barPressureProvider.getPressure() * 10000;
    }
}
