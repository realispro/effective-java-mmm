package com.mmm.ej.gof.structural.proxy.image;

public class ImageFile implements Image{

    private byte[] data;

    public ImageFile(String fileName){
        System.out.println("loading data from file " + fileName);
        data = fileName.getBytes(); // data loading emulation
    }


    @Override
    public byte[] getData() {
        return data;
    }
}
