package com.mmm.ej.gof.structural.proxy.net;

import com.mmm.ej.gof.structural.proxy.net.internet.RealNetwork;

public class InternetProvider {

    public static Internet provide(){
        return new InternetProxy(new InternetAuditProxy(new RealNetwork()));
    }

}
