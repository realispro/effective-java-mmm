package com.mmm.ej.solid;

import java.util.Random;

public class Manager extends Employee implements Management{

    public Manager(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void performAssesment(Employee employee){
        int rate = new Random().nextInt(10)+1;
        if(rate>6){
            employee.setGrade(employee.getGrade()+1);
        }
    }


}
