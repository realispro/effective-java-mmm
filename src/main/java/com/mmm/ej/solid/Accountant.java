package com.mmm.ej.solid;

public class Accountant extends Employee {


    public Accountant(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void performAccounting(){
        System.out.println("accountant " + firstName + " " + lastName + " is counting");
    }


}
