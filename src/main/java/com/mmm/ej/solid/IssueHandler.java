package com.mmm.ej.solid;

@FunctionalInterface
public interface IssueHandler {
    void handleIssue(String issue, IssueHandler specialist);

}
