package com.mmm.ej.solid;

public class Specialist extends Employee implements IssueHandler {

    public Specialist(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void doMagic(){
        System.out.println("specialist " + firstName + " " + lastName + " is doing magic");
    }

    @Override
    public void handleIssue(String issue, IssueHandler specialist){
        System.out.println("specialist is handling issue:" + issue);
    }

}
