package com.mmm.ej.solid;

public class SolidMain {



    public static void main(String[] args){
        System.out.println("SOLID"); //sout

        Manager manager = new Manager("Adam", "Nowak", 8);

        Specialist specialist = new Specialist("Joe", "Doe", 5);
        specialist.doMagic();
        
        Accountant accountant = new Accountant("Jan", "Kowalski", 4);
        accountant.performAccounting();

        String issue = "critical issue";
        IssueHandler issueHandler = new Intern();
        IssueHandler issueHandler2 = new Intern();
        issueHandler.handleIssue(issue, issueHandler2);


        Management management = manager;
                //new Director("Krzystzof", "Kajak");
        management.sendAnnouncement("annual assessment started");

        manager.performAssesment(specialist);
        manager.performAssesment(accountant);

        System.out.println("specialist = " + specialist);
        System.out.println("accountant = " + accountant);

    }


}
