package com.mmm.ej.solid;

public class Intern implements IssueHandler{
    @Override
    public void handleIssue(String issue, IssueHandler specialist) {
        System.out.println("intern is handling issue: " + issue);

    }
}
