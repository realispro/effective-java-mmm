package com.mmm.ej.solid;

public interface Management {

    default void sendAnnouncement(String message) {
        System.out.println("announcement: " + message);
    }
}
